'use strict';

(function() {
    $('.button').on('click', function(){
        $('.hidden-info').toggleClass('active');
        $(this).text(function(i, str){
            return str === "Show more" ? "Show less" : "Show more";
        })
    });
}) ();
