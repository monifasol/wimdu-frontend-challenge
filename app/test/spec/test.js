/* global describe, it */

(function () {
  'use strict';

    describe('testing load of fixtures', function () {
        it('should load my fixture into DOM', function () {
            loadFixtures('my-fixture.html');
            expect($('#my-fixture')).toHaveClass("testing");
        });
    });

    describe("Trigger click event in the show-hide Button", function() {
        var spyEvent;
        beforeEach(function () {
            loadFixtures('my-fixture.html');
        });

        it('should load the show-hide button', function () {
            expect($('.button')).toExist();
        });

        it ("should invoke the button click event.", function() {
            spyEvent = spyOnEvent('.button', 'click');
            $('.button').trigger('click');

            expect('click').toHaveBeenTriggeredOn('.button');
            expect(spyEvent).toHaveBeenTriggered();
        });
    });

    describe("At first...", function() {
        beforeEach(function () {
            loadFixtures('my-fixture.html');
        });

        it ("should be hidden the additional info", function() {
            expect($('.hidden-info').hasClass('active')).toBe(false);
            expect($(".hidden-info")).toHaveCss({"visibility": "hidden"});
            //.toBeVisible() works only for display:none
        });

        it ("should display 'Show more' in the button", function() {
            expect($('.button')).toHaveText('Show more');
        });

    });

    describe("When the button is clicked once", function() {
        var spyEvent;

        beforeEach(function () {
            loadFixtures('my-fixture.html');
            spyEvent = spyOnEvent('.button', 'click');
            $('.button').trigger('click');
        });

        it ("should add the class 'active' to the additional info div", function() {
            expect($(".hidden-info")).hasClass('active').toBe(true);
        });

        it ("should show the additional info div", function() {
            expect($(".hidden-info")).toHaveCss({"visibility": "visible"});
        });

        it ("should display the correct title of the additional info", function() {
            expect($('.hidden-info .title')).toHaveText("House rules");
        });

        it ("should display the correct text of the additional info", function() {
            expect($(".hidden-info")).toHaveText($('.hidden-info-paragraph').val());
        });

        it ("should display 'Show less' in the button", function() {
            expect($('.button')).toHaveText('Show less');
        });
    });

    describe("When the button is clicked twice", function() {
        var spyEvent;
        beforeEach(function () {
            loadFixtures('my-fixture.html');
            spyEvent = spyOnEvent('.button', 'click');
            $('.button').trigger('click');
            $('.button').trigger('click');
        });

        it ("should remove the class 'active' to the additional info div", function() {
            expect($('.hidden-info').hasClass('active')).toBe(false);
        });

        it ("should hide the additional info div", function() {
            expect($(".hidden-info")).toHaveCss({"visibility": "hidden"});
        });

        it ("should display 'Show more' in the button", function() {
            expect($('.button')).toHaveText('Show more');
        });
    });
})();

// My callback on the onclick function is not being executed.
// I think I should look at asynchronous calls for that.